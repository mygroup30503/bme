package com.basketball.modern.app.era.presentation.screens.waiting

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.basketball.modern.app.era.R
import com.basketball.modern.app.era.presentation.utils.BMEScreens
import kotlinx.coroutines.delay

@Composable
fun BMEWaitingScreen(navHostController: NavHostController) {
    LaunchedEffect(key1 = true) {
        delay((0..2000L).random())
        navHostController.navigate(BMEScreens.BME_HOME) {
            popUpTo(BMEScreens.BME_WAITING) { inclusive = true }
        }
    }
    Box(modifier = Modifier.padding(horizontal = 32.dp).fillMaxSize(), contentAlignment = Alignment.Center) {
        Image(painter = painterResource(id = R.drawable.load), contentDescription = null)
    }
}