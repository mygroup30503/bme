package com.basketball.modern.app.era.di

import com.basketball.modern.app.era.domain.repository.BMEPlayerRepository
import com.basketball.modern.app.era.data.repository.BMEPlayerRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BMEAppModule {
    @Provides
    @Singleton
    fun providedBMEPlayerRepository(): BMEPlayerRepository{
        return BMEPlayerRepositoryImpl()
    }
}