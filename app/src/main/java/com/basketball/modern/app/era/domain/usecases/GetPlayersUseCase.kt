package com.basketball.modern.app.era.domain.usecases


import com.basketball.modern.app.era.domain.repository.BMEPlayerRepository
import com.basketball.modern.app.era.data.player.PlayerInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetPlayersUseCase @Inject constructor(
    private val repository: BMEPlayerRepository
) {
    private val _data = MutableStateFlow(repository.getFirstPlayer())
    val data: StateFlow<PlayerInfo> = _data
    suspend fun getNext(){
        repository.getNextPlayer()?.let { _data.emit(it) }
    }

    suspend fun getLast(){
        repository.getLastPlayer()?.let { _data.emit(it) }
    }
}