package com.basketball.modern.app.era.data.player

data class PlayerInfo(
    val playerName: String,
    val playerCard: Int,
    val playerBio: String
)
