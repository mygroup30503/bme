package com.mundojogo.info.app.holdem.presentation.screens.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.basketball.modern.app.era.domain.usecases.GetPlayersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BMEWaitingScreenViewModel @Inject constructor(
    val getPlayerUseCase: GetPlayersUseCase
): ViewModel() {
    val t = getPlayerUseCase.data

    fun getNext(){
        viewModelScope.launch {
            getPlayerUseCase.getNext()
        }

    }

    fun getLast(){
        viewModelScope.launch {
            getPlayerUseCase.getLast()
        }
    }

}