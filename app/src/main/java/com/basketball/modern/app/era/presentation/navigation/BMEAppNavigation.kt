package com.basketball.modern.app.era.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.basketball.modern.app.era.presentation.screens.home.BMEHomeScreen
import com.basketball.modern.app.era.presentation.screens.players.BMEPlayersScreen
import com.basketball.modern.app.era.presentation.utils.BMEScreens
import com.basketball.modern.app.era.presentation.screens.waiting.BMEWaitingScreen

@Composable
fun BMEAppNavigation() {
    val navHostController = rememberNavController()
    NavHost(
        navController = navHostController,
        startDestination = BMEScreens.BME_WAITING
    ) {
        composable(BMEScreens.BME_HOME){
            BMEHomeScreen(navHostController = navHostController)
        }
        composable(BMEScreens.BME_WAITING){
            BMEWaitingScreen(navHostController = navHostController)
        }
        composable(BMEScreens.BME_PLAYERS){
            BMEPlayersScreen(navHostController = navHostController)
        }
    }
}