package com.basketball.modern.app.era.domain.repository

import com.basketball.modern.app.era.data.player.PlayerInfo

interface BMEPlayerRepository {
    fun getFirstPlayer(): PlayerInfo
    fun getNextPlayer(): PlayerInfo?
    fun getLastPlayer(): PlayerInfo?
}