package com.basketball.modern.app.era.data.storage

import com.basketball.modern.app.era.R
import com.basketball.modern.app.era.data.player.PlayerInfo

object BasketballPlayerCollection {
    val bmeList = arrayOf(
        PlayerInfo(
            "Josh Giddey",
            R.drawable.player_4,
            "In his rookie outing, Josh Giddey showed flashes of true brilliance on the basketball court. Arguably one of the best pure passers in the league already, Giddey put up incredible stats for a rookie, including 12.5 points per game with 7.8 rebounds and 6.4 assists. In summer league, Giddey looked more than ready for the season, dishing out beautiful dimes. Giddey needs to become a bit more poised on the offensive end of the ball, with 42 percent from the field and an abysmal 26 percent from the three just not cutting it. Giddey showed determination last season, and if he comes back with a refined jumpshot on top of the tools he already has, the league should be very worries."
        ),
        PlayerInfo(
            "LaMelo Ball",
            R.drawable.player_5,
            "Entering his third year in the NBA, LaMelo Ball has already solidified himself as a star in the league. Averaging 18.3 points, 7 assists, and 6.4 rebounds for his career, LaMelo has made it clear during the offseason that he wants, and needs, to win. While the team in Charlotte may have lost some key pieces, LaMelo looks more than ready to pick up the slack and help finally lead his team to the postseason. LaMelo is already looking like a man on a mission this offseason, and if he can take the jump from star to legit All-Star talent, the NBA world will most certainly be put on notice."
        ),
        PlayerInfo(
            "Deandre Ayton",
            R.drawable.player_6,
            "Deandre Ayton had arguably the most tumultuous and bizarre postseason of any player in the NBA during the 2022 offseason. After his coach put him on blast, the team owner not wanting to pay him, and his teammates seemingly being nowhere to be found to support him, Ayton most certainly has something to prove coming into the season. Ayton has been a steady and reliable contributor for the Phoenix Suns during his 4 years with the team, be with Chris Paul's rapid decline, Ayton will need to pick up the slack, and with he himself clamoring for wanting more touches and responsibility on the team, he will get that opportunity. After the Suns got embarrassed in the playoffs, many have written them off, but if Ayton can prove that he is a max player, then they very well could come back this season with a vengance."
        ),
        PlayerInfo(
            "Michael Porter Jr.",
            R.drawable.player_7,
            "After a hellacious outing during the 2020-21 regular and postseason, Michael Porter Jr. looked poised to elevate himself into All-Star status in the NBA, and then after a truly lackluster 9 games to start the 2021-22 season, Porter Jr. would injure his back and miss the rest of the regular season. The 9 games many saw tempered the idea that Porter Jr. would ever become an All-Star, let alone star in the NBA, and after back surgery, all notions seem laughable. Porter Jr. has a lot of critics to answer and a lot to prove not only to the NBA, but to himself. If he is able to get back to what he once was, a uber-talented scorer on a winning team, with the redemption story featured in, Porter Jr. will undoubtedly have the narrative behind him for most improved player."
        ),
        PlayerInfo(
            "Christian Wood",
            R.drawable.player_8,
            "Christian wood has been quite the journeyman in his first 6 seasons in the NBA, already playing for 6 teams during those 6 years. Wood was traded to the Mavericks, making his 7th team in 7 years, and while his career was rocky to start, Wood has grown into a fantastic stretch 5 in the NBA. With Luka Doncic dishing out dimes to Wood, his stats across the board should greatly improve, and playing with one of the most talented players in the league is sure to help his career, and help him improve drastically as a player."
        ),
        PlayerInfo(
            "Boban Marjanović",
            R.drawable.player_9,
            "Standing at a towering 7-foot-4, Serbian center Boban Marjanovic has captured the imagination and hearts of fans with his gigantic presence on the court and magnetic, affable demeanor off it. With an undeniable aptitude for scoring inside, securing rebounds, and altering shots, Boban's unique combination of skills and physical traits render him both an effective player and a beloved figure across the league. Beyond his basketball prowess, Marjanovic's charming personality has made him a veritable social media superstar, further cementing his status as an NBA fan-favorite."
        ),
        PlayerInfo(
            "Derrick Rose",
            R.drawable.player_10,
            "Derrick Rose's career represents an inspiring story of perseverance and resilience, as he has weathered multiple severe injuries to continually reinvent himself as an effective and impactful player. Known for his electrifying speed and acrobatic scoring ability, Rose has endeared himself to basketball aficionados with his fearless style of play and unwavering determination to succeed. As he continues to thrive against adversity, Rose's unbreakable spirit and relentless passion serve as a testament to the human potential for growth and triumph."
        ),
        PlayerInfo(
            "Steven Adams",
            R.drawable.player_11,
            "New Zealand's Steven Adams is a gritty and tenacious force in the paint, utilizing his imposing 7-foot frame and impressive strength to dominate on the boards and finish powerfully at the rim. As a loyal, hardworking, and team-first player, Adams embodies the spirit of unselfish basketball and has emerged as a crucial component of his team's success. With his laid-back demeanor, dry wit, and distinctive appearance, Adams has quickly become a darling among NBA enthusiasts."
        ),
        PlayerInfo(
            "Damian Lillard",
            R.drawable.player_12,
            "Known for his clutch performances and ice-cold late-game heroics, Damian Lillard's penchant for hitting game-winning shots has rightfully earned him the nickname \"Dame Time.\" A fearless and determined leader on the court, Lillard combines his elite scoring ability with a humble, team-first mentality that greatly endears him to fans. Off the court, his passion for community and philanthropy further solidifies his status as one of the league's most likable players."
        ),
        PlayerInfo(
            "DeMar DeRozan",
            R.drawable.player_13,
            "DeMar DeRozan's style of play, marked by smooth mid-range shooting, powerful drives to the rim, and quietly efficient scoring, has made him a fan favorite in both Toronto and now San Antonio. His humble and professional demeanor, coupled with his willingness to embrace and grow within his ever-changing roles, endears him to fans, teammates, and coaches alike. Off the court, DeRozan's openness about mental health issues has destigmatized the conversation surrounding mental well-being and solidified him as an influential and relatable figure in the basketball world."
        ),
        PlayerInfo(
            "Steph Curry",
            R.drawable.player_14,
            "Steph Curry's dazzling displays of long-range shooting, quick handles, and flashy passes have not only made him one of the most entertaining players to watch but also redefined what is considered possible on the basketball court. His humble beginnings as an overlooked college recruit and subsequent rise to superstardom have endeared him to fans worldwide, while his infectious smile and charismatic personality make him all the more likable. Curry's impact on the game, coupled with his commitment to philanthropy and community initiatives, cements him as an exemplary ambassador for the league."
        ),
        PlayerInfo(
            "Tacko Fall",
            R.drawable.player_15,
            "An imposing figure standing at 7-foot-5, Tacko Fall's sheer size and unique skill set make him a fascinating outlier within the NBA universe. Born in Senegal, Fall's journey from youth soccer player to collegiate sensation and now an NBA prospect has captured the hearts of many who appreciate his dedication to learning the game, raw potential, and unassuming personality. As a fan favorite, his every appearance on the court is met with cheers, proving that his draw extends far beyond his towering stature."
        ),
        PlayerInfo(
            "Donovan Mitchell",
            R.drawable.player_16,
            "Donovan Mitchell's meteoric rise to stardom is characterized by his explosive athleticism, dynamic scoring ability, and unwavering competitiveness. Mitchell's fearless approach to the game and indomitable spirit have earned him the respect and admiration of fans and peers alike, while his engaging personality and infectious smile only serve to heighten his appeal. Off the court, Donovan's commitment to community service and his vocal advocacy for positive change further bolster his status as a universally admired figure."
        ),
        PlayerInfo(
            "Bol Bol",
            R.drawable.player_17,
            "Carrying a unique legacy as the son of former NBA player Manute Bol, Bol Bol's intriguing blend of size, agility, and shooting touch provides an electrifying presence on the court. Standing 7-foot-2, his incredible wingspan coupled with his burgeoning skill set makes him a captivating force in the paint and beyond. As he continues to develop, Bol's exciting potential and affable nature make him an up-and-coming fan favorite."
        ),
        PlayerInfo(
            "Alex Caruso",
            R.drawable.player_18,
            "The quintessential underdog story, Alex Caruso's unlikely rise from undrafted rookie to crucial rotation player has made him a fan favorite among die-hard basketball enthusiasts. Known for his hustle, grit, and high-flying dunks, Caruso's blue-collar approach to the game and fearless attitude have endeared him to spectators far and wide. As he continues to defy expectations and prove his worth on the NBA's grandest stage, Caruso's unwavering determination and self-belief serve as an inspiration to all."
        ),

    )
}