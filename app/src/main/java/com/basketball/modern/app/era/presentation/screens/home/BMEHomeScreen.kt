package com.basketball.modern.app.era.presentation.screens.home

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.basketball.modern.app.era.R
import com.basketball.modern.app.era.presentation.utils.BMEScreens

@Composable
fun BMEHomeScreen(navHostController: NavHostController) {
    val mainActivity = (LocalContext.current as? Activity)
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Box(modifier = Modifier.weight(1f))
            Column(modifier = Modifier.weight(3f)) {
                Image(painter = painterResource(id = R.drawable.start_button),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            navHostController.navigate(BMEScreens.BME_PLAYERS)
                        }, contentScale = ContentScale.FillWidth)
                Spacer(modifier = Modifier.height(40.dp))
                Image(painter = painterResource(id = R.drawable.exit_button),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            mainActivity?.finish()
                        }, contentScale = ContentScale.FillWidth)
            }
            Box(modifier = Modifier.weight(1f))
        }
    }
}