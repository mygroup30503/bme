package com.basketball.modern.app.era.data.repository

import com.basketball.modern.app.era.data.storage.BasketballPlayerCollection
import com.basketball.modern.app.era.data.player.PlayerInfo
import com.basketball.modern.app.era.domain.repository.BMEPlayerRepository

class BMEPlayerRepositoryImpl(): BMEPlayerRepository {

    private val mainList = BasketballPlayerCollection.bmeList

    private val listSize = mainList.size

    private var iterator = 0

    override fun getFirstPlayer(): PlayerInfo {
        iterator=0
        return mainList.first()
    }

    override fun getNextPlayer(): PlayerInfo? {
        if (iterator>=listSize-1) return null
        iterator++
        return mainList[iterator]
    }

    override fun getLastPlayer(): PlayerInfo? {
        if (iterator<=0) return null
        iterator--
        return mainList[iterator]
    }
}