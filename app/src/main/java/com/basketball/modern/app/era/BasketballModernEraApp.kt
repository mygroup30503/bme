package com.basketball.modern.app.era

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BasketballModernEraApp: Application()