package com.basketball.modern.app.era.presentation.screens.players

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.basketball.modern.app.era.R
import com.basketball.modern.app.era.data.player.PlayerInfo
import com.basketball.modern.app.era.presentation.utils.Fonts
import com.mundojogo.info.app.holdem.presentation.screens.info.BMEWaitingScreenViewModel

@Composable
fun BMEPlayersScreen(navHostController: NavHostController) {
    val vm: BMEWaitingScreenViewModel = hiltViewModel()
    val players = vm.t.collectAsState()
    InfoPart(data = players.value, navHostController = navHostController, {
        vm.getNext()
    }, {
        vm.getLast()
    })


}

@Composable
fun InfoPart(
    data: PlayerInfo,
    navHostController: NavHostController,
    onNext: () -> Unit,
    onLast: () -> Unit
) {
    val scrolling = rememberScrollState()
    Column(modifier = Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            Image(painter = painterResource(id = R.drawable.icon_back), contentDescription = null,
                modifier = Modifier
                    .clickable {
                        onLast.invoke()
                    }
                    .weight(1f)
                    .padding(8.dp)
            )
            Text(
                text = data.playerName, modifier = Modifier
                    .weight(5f)
                    .fillMaxHeight(), style = TextStyle(
                    color = Color.White,
                    fontFamily = Fonts.font,
                    textAlign = TextAlign.Center,
                    fontSize = 22.sp
                )
            )
            Box(modifier = Modifier.weight(1f))
        }
        Column(
            modifier = Modifier
                .weight(7f)
                .verticalScroll(scrolling)

        ) {
            Image(
                painter = painterResource(id = data.playerCard),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillWidth
            )
            Text(text = data.playerBio, modifier = Modifier.padding(16.dp),
                style = TextStyle(
                    fontSize = 16.sp, color = Color.White, fontFamily = Fonts.font
                )
            )
        }
        Box(
            modifier = Modifier
                .padding(vertical = 4.dp, horizontal = 32.dp)
                .fillMaxWidth()
                .weight(1f),
            contentAlignment = Alignment.Center
        ) {
            if (data.playerName != "Alex Caruso") Image(
                painter = painterResource(id = R.drawable.next_button),
                contentDescription = null,
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .fillMaxHeight()
                    .clickable {
                        onNext.invoke()
                    },
                contentScale = ContentScale.FillHeight
            ) else Image(
                painter = painterResource(id = R.drawable.menu_button),
                contentDescription = null,
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .fillMaxHeight()
                    .clickable {
                        navHostController.popBackStack()
                    },
                contentScale = ContentScale.FillHeight
            )
        }
    }
}


