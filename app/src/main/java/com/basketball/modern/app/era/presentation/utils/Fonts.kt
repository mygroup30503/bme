package com.basketball.modern.app.era.presentation.utils

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import com.basketball.modern.app.era.R


object Fonts {
    val font = FontFamily(Font(R.font.font))
}